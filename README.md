# Opus UI News Feed Open Source

UI prototype of news feed section of a typical social networking app
## Installation

1. Clone the git repository
2. Connect to a Firebase project
3. Add google-services.json file
4. Setup Google sign-in in Firebase

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)