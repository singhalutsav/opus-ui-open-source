package in.praxiv.fbtest;

import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.devs.readmoreoption.ReadMoreOption;
import com.rd.PageIndicatorView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.recyclerview.widget.RecyclerView;

public class MyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<NewsFeedModel.DataBean> modelList = new ArrayList<>();
    private ReadMoreOption readMoreOption;
    private Handler handler;
    private Runnable runnable;

    public MyAdapter(Context context, Handler handler){
        readMoreOption = new ReadMoreOption.Builder(context)
                //.textLength(3, ReadMoreOption.TYPE_LINE) // OR
                .textLength(150, ReadMoreOption.TYPE_CHARACTER)
                .moreLabel("View More")
                .lessLabel("View Less")
                .moreLabelColor(Color.BLUE)
                .lessLabelColor(Color.BLUE)
                .labelUnderLine(true)
                .build();
        runnable = (Runnable)context;
        this.handler = handler;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public Context context;
        public View mainView;
        public TextView nameLocationTextView;
        public ImageView userImageView;
        public TextView dateTextView;
        public TextView contentTextView;
        public ImageView picImageView;
        public FrameLayout picContainer;
        public ImageView leftScrollImageView;
        public ImageView rightScrollImageView;
        public ProgressBar progressBar;
        public PageIndicatorView pageIndicatorView;

        public MyViewHolder(View view) {
            super(view);

            mainView = view;

            nameLocationTextView = view.findViewById(R.id.name_location_textView);
            userImageView = view.findViewById(R.id.user_imageView);
            dateTextView = view.findViewById(R.id.date_textView);
            contentTextView = view.findViewById(R.id.content_textView);
            picImageView = view.findViewById(R.id.pic_imageView);
            picContainer = view.findViewById(R.id.picContainer);
            leftScrollImageView = view.findViewById(R.id.leftScrollImageView);
            rightScrollImageView = view.findViewById(R.id.rightScrollImageView);
            progressBar = view.findViewById(R.id.progressBar);
            pageIndicatorView = view.findViewById(R.id.pageIndicatorView);
            context = mainView.getContext();
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.news_feed_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holderObj, final int position) {
        final MyViewHolder holder = (MyViewHolder)holderObj;

        if(modelList.size() == 0){
            return;
        }

        final NewsFeedModel.DataBean data = modelList.get(position);
        if(!data.getLoc_name().isEmpty()){
            String text = "<b>" + data.getPost_user_name() + "</b> was at <b>" + data.getLoc_name() + "</b>";
            holder.nameLocationTextView.setText(Html.fromHtml(text));
        } else{
            holder.nameLocationTextView.setText(Html.fromHtml("<b>" + data.getPost_user_name() + "</b>"));
        }
        String createdAt = data.getCreated_at();
        holder.dateTextView.setText(createdAt.substring(0, createdAt.length() - 3));

        String content = data.getPost_content() + "  ";
        readMoreOption.addReadMoreTo(holder.contentTextView, content);
        //holder.contentTextView.setText(content);
        if(TextUtils.isEmpty(content)){
            holder.contentTextView.setVisibility(View.GONE);
        } else {
            holder.contentTextView.setVisibility(View.VISIBLE);
        }

        holder.rightScrollImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(runnable, 3000);
                int n = holder.getAdapterPosition();
                NewsFeedModel.DataBean bean = modelList.get(n);
                List<NewsFeedModel.DataBean.PostMediaBean> mediaList = bean.getPost_media();
                bean.currentMediaItem++;
                if(mediaList.size() <= bean.currentMediaItem)
                    bean.currentMediaItem = 0;
                notifyItemChanged(n);
            }
        });

        holder.leftScrollImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handler.removeCallbacksAndMessages(null);
                handler.postDelayed(runnable, 3000);
                int n = holder.getAdapterPosition();
                NewsFeedModel.DataBean bean = modelList.get(n);
                List<NewsFeedModel.DataBean.PostMediaBean> mediaList = bean.getPost_media();
                bean.currentMediaItem--;
                if(bean.currentMediaItem < 0)
                    bean.currentMediaItem = mediaList.size() - 1;
                notifyItemChanged(n);
            }
        });


        Picasso.get()
                .load(data.getPost_user_pic())
                //.resize(50, 50)
                .placeholder(R.drawable.ic_person)
                .fit()
                .centerCrop()
                .into(holder.userImageView);

        List<NewsFeedModel.DataBean.PostMediaBean> mediaList = data.getPost_media();
        if(mediaList == null || mediaList.size() == 0){
            holder.picContainer.setVisibility(View.GONE);
            holder.leftScrollImageView.setVisibility(View.GONE);
            holder.rightScrollImageView.setVisibility(View.GONE);
            holder.progressBar.setVisibility(View.GONE);
            holder.pageIndicatorView.setVisibility(View.GONE);
        } else if(mediaList.size() == 1){
            holder.picContainer.setVisibility(View.VISIBLE);
            holder.leftScrollImageView.setVisibility(View.GONE);
            holder.rightScrollImageView.setVisibility(View.GONE);
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.pageIndicatorView.setVisibility(View.GONE);
            Picasso.get()
                    .load(mediaList.get(0).getMedia_url())
                    //.placeholder(R.drawable.loading)
                    .error(R.drawable.thumbnail_na)
                    .fit()
                    .centerCrop()
                    .into(holder.picImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    });
        } else {
            holder.picContainer.setVisibility(View.VISIBLE);
            holder.leftScrollImageView.setVisibility(View.VISIBLE);
            holder.rightScrollImageView.setVisibility(View.VISIBLE);
            holder.progressBar.setVisibility(View.VISIBLE);
            holder.pageIndicatorView.setVisibility(View.VISIBLE);
            holder.pageIndicatorView.setCount(mediaList.size());
            holder.pageIndicatorView.setSelection(data.currentMediaItem);
            Picasso.get()
                    .load(mediaList.get(data.currentMediaItem).getMedia_url())
                    //.placeholder(R.drawable.loading)
                    .error(R.drawable.thumbnail_na)
                    .fit()
                    .centerCrop()
                    .into(holder.picImageView, new Callback() {
                        @Override
                        public void onSuccess() {
                            holder.progressBar.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError(Exception e) {
                            holder.progressBar.setVisibility(View.GONE);
                        }
                    });
        }
    }

    @Override
    public int getItemCount() {
        return modelList.size();
    }

    public void updateDataset(List<NewsFeedModel.DataBean> data){
        modelList.clear();
        modelList.addAll(data);
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
