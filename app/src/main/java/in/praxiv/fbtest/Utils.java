package in.praxiv.fbtest;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class Utils {
    private static ApiService API_SERVICE = null;

    private static void initApiRetrofit() {
        String baseUrl = "https://www.whats42nite.com/production/apiv2/";
        if (API_SERVICE == null) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient.Builder httpClient =
                    new OkHttpClient.Builder()
                            //.addInterceptor(interceptor)
                            .connectTimeout(100, TimeUnit.SECONDS)
                            .readTimeout(100, TimeUnit.SECONDS);

            Retrofit retrofit =
                    new Retrofit.Builder()
                            .baseUrl(baseUrl)
                            .client(httpClient.build())
                            .addConverterFactory(ScalarsConverterFactory.create())
                            .addConverterFactory(GsonConverterFactory.create()).build();

            API_SERVICE = retrofit.create(ApiService.class);
        }
    }

    public static void getPosts(Context context, ProgressBar progressBar,
                             ApiResponseListener listener, int userId, int venueId) {

        initApiRetrofit();

        if(progressBar != null)
            progressBar.setVisibility(View.VISIBLE);

        Call<String> call = API_SERVICE.getPosts("https://sanathan-dharma.firebaseio.com/response.json");
        call.enqueue(new MyCallback(context, progressBar, listener));
    }


}
