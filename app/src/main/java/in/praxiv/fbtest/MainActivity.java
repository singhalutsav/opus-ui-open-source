package in.praxiv.fbtest;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import in.praxiv.fbtest.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements Runnable{

    private ActivityMainBinding binding;
    private NewsFeedModel model;
    private MyAdapter adapter;
    private LinearLayoutManager layoutManager;
    private Handler handler = new Handler();
    private List<NewsFeedModel.DataBean> adapterList;
    private int currentPosition;

    private ApiResponseListener listener = new ApiResponseListener() {
        @Override
        public void respond(String responseBody) {
            //Toast.makeText(MainActivity.this, responseBody, Toast.LENGTH_SHORT).show();
            Log.v("TagTest", responseBody);

            Gson gson = new Gson();
            try{
                model = gson.fromJson(responseBody, NewsFeedModel.class);
            } catch(RuntimeException e){
                e.printStackTrace();
                Toast.makeText(MainActivity.this, responseBody, Toast.LENGTH_SHORT).show();
                return;
            }

            adapterList.clear();
            adapterList.addAll(model.getData());
            adapter.updateDataset(adapterList);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.toolbar);

        binding.fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        adapterList = new ArrayList<>();
        setUpRecyclerView();

        binding.contentMain.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                handler.removeCallbacksAndMessages(null);
                currentPosition = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(currentPosition == -1)
                    currentPosition = layoutManager.findFirstVisibleItemPosition();
                if(currentPosition == -1)
                    currentPosition = layoutManager.findLastVisibleItemPosition();
                if(currentPosition == -1)
                    currentPosition = layoutManager.findLastCompletelyVisibleItemPosition();
                Log.v("TagTest2", "Current position: " + currentPosition);
                handler.postDelayed(MainActivity.this, 3000);
            }
        });

        Utils.getPosts(this, binding.contentMain.progressBar, listener, 885, 432);
    }

    private void setUpRecyclerView(){
        adapter = new MyAdapter(this, handler);
        binding.contentMain.recyclerView.setAdapter(adapter);
        layoutManager = new LinearLayoutManager(this);
        binding.contentMain.recyclerView.setLayoutManager(layoutManager);
        binding.contentMain.recyclerView.setHasFixedSize(true);
//        DividerItemDecoration dividerItemDecoration =
//                new DividerItemDecoration(binding.contentMain.recyclerView.getContext(),
//                layoutManager.getOrientation());
//        binding.contentMain.recyclerView.addItemDecoration(dividerItemDecoration);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if(id == R.id.action_logout){
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(MainActivity.this, "Logging Out, Please Wait", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);

                        }
                    });
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void run() {
        if(adapterList.size() == 0)
            return;
        if(currentPosition == -1)
            return;
        NewsFeedModel.DataBean data = adapterList.get(currentPosition);
        List<NewsFeedModel.DataBean.PostMediaBean> mediaList = data.getPost_media();
        if(mediaList == null || mediaList.size() == 0 || mediaList.size() == 1){
            handler.postDelayed(this, 3000);
            return;
        }

        data.currentMediaItem++;
        if(data.getPost_media().size() <= data.currentMediaItem)
            data.currentMediaItem = 0;
        adapter.notifyItemChanged(currentPosition);
        Log.v("TagTest", "Current position in run(): " + currentPosition);
        handler.postDelayed(this, 3000);
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        handler.post(this);
    }
}
