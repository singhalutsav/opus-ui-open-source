package in.praxiv.fbtest;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ApiService {
    @GET
    Call<String> getPosts(@Url String url);
}
