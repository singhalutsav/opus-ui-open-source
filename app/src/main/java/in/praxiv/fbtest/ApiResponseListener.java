package in.praxiv.fbtest;

public interface ApiResponseListener {
    public void respond(String responseBody);
}
