package in.praxiv.fbtest;

import java.util.List;

public class NewsFeedModel {

    /**
     * app_version : {"android":"9","ios":"8","is_force_update":"0"}
     * data : [{"comment_count":"3","created_at":"2020-04-30 21:05:46","created_at_timestamp":"1588305946","is_commented":"1","is_liked":"1","is_shared":"1","is_viewed":"1","like_count":"2","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"Testing","post_created_user_id":"1084","post_created_user_role":"App","post_id":"1477","post_media":[{"media_height":800,"media_type":"image","media_url":"https://images.newindianexpress.com/uploads/user/imagelibrary/2020/1/25/w900X450/Ministry-sports.jpg","media_width":599}],"post_type":"1","post_user_id":"1084","post_user_name":"Utsav Singhal","post_user_pic":"https://cms.qz.com/wp-content/uploads/2019/07/IMG_8407-copy-e1563479651241.jpg?quality=75&strip=all&w=1600&h=900&crop=1","post_user_role":"App","share_count":"21","share_type":"0","unique_id":"ROf7sl","updated_at":"2020-05-08 05:12:06","view_count":"22"},{"comment_count":"0","created_at":"2020-04-29 10:41:32","created_at_timestamp":"1588182092","is_commented":"0","is_liked":"1","is_shared":"1","is_viewed":"1","like_count":"1","loc_address":"New Delhi","loc_latitude":"28.5515834","loc_longitude":"77.1207082","loc_name":"India Gate","mime_type":"2","post_content":"Test post with images  and location","post_created_user_id":"1084","post_created_user_role":"App","post_id":"1476","post_media":[{"media_height":800,"media_type":"image","media_url":"https://static.toiimg.com/photo/45108945/.jpg","media_width":599}],"post_type":"1","post_user_id":"1084","post_user_name":"Utsav Singhal","post_user_pic":"https://cms.qz.com/wp-content/uploads/2019/07/IMG_8407-copy-e1563479651241.jpg?quality=75&strip=all&w=1600&h=900&crop=1","post_user_role":"App","share_count":"3","share_type":"0","unique_id":"vECHS0","updated_at":"2020-05-08 05:31:41","view_count":"22"},{"comment_count":"0","created_at":"2020-04-26 08:50:13","created_at_timestamp":"1587916213","is_commented":"0","is_liked":"1","is_shared":"1","is_viewed":"1","like_count":"1","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"Test Post with image","post_created_user_id":"1084","post_created_user_role":"App","post_id":"1475","post_media":[{"media_height":800,"media_type":"image","media_url":"https://pathik.estrick.com/wp-content/uploads/2019/09/Top-Waterparks-In-India.jpg","media_width":599}],"post_type":"1","post_user_id":"1084","post_user_name":"Anuj Singhal","post_user_pic":"https://static.toiimg.com/photo/msid-64906749/64906749.jpg?resizemode=4&width=400","post_user_role":"App","share_count":"1","share_type":"0","unique_id":"DGlr5E","updated_at":"2020-05-08 05:32:43","view_count":"22"},{"comment_count":"1","created_at":"2020-04-25 12:27:46","created_at_timestamp":"1587842866","is_commented":"0","is_liked":"1","is_shared":"1","is_viewed":"1","like_count":"2","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"1","post_content":"Test post","post_created_user_id":"1098","post_created_user_role":"App","post_id":"1474","post_type":"1","post_user_id":"1098","post_user_name":"Deepak Gupta","post_user_pic":"http://graph.facebook.com/2607693405962209/picture?width=500&height=500","post_user_role":"App","share_count":"3","share_type":"0","unique_id":"d2gHAT","updated_at":"2020-05-07 17:50:06","view_count":"22"},{"comment_count":"3","created_at":"2020-04-22 12:37:06","created_at_timestamp":"1587584226","is_commented":"0","is_liked":"0","is_shared":"0","is_viewed":"1","like_count":"1","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"Testing posting time on post with 3 images.","post_created_user_id":"1085","post_created_user_role":"App","post_id":"1473","post_media":[{"media_height":1233,"media_type":"image","media_url":"https://www.india-briefing.com/news/wp-content/uploads/2019/05/India-Briefing-Indias-Top-Investment-Destinations.jpg","media_width":599},{"media_height":1233,"media_type":"image","media_url":"https://images.livemint.com/rf/Image-621x414/LiveMint/Period2/2019/01/15/Photos/Home%20Page/urbanindiamint-kBiB--621x414@LiveMint.JPG","media_width":599},{"media_height":1233,"media_type":"image","media_url":"https://www.raconteur.net/wp-content/uploads/2016/11/India-smart-cities.jpg","media_width":599}],"post_type":"1","post_user_id":"1085","post_user_name":"Shikha Goel","post_user_pic":"https://1.bp.blogspot.com/-Wp41XE9I36Y/XO1HUDLw9cI/AAAAAAAAAA8/iLlYu16n2kMmONj3CLfsWfo5v9SzAQgsQCLcBGAs/s1600/Screenshot_20180917-001149.png","post_user_role":"App","share_count":"0","share_type":"0","unique_id":"HaDr1E","updated_at":"2020-05-07 15:45:58","view_count":"23"},{"comment_count":"0","created_at":"2020-04-22 08:27:57","created_at_timestamp":"1587569277","is_commented":"0","is_liked":"0","is_shared":"1","is_viewed":"1","like_count":"1","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"1","post_content":"Test.","post_created_user_id":"1085","post_created_user_role":"App","post_id":"1472","post_type":"1","post_user_id":"1085","post_user_name":"Mohit Goel","post_user_pic":"https://i.pinimg.com/originals/00/b8/d5/00b8d5890f454e510a3f540ea9e09a0d.jpg","post_user_role":"App","share_count":"1","share_type":"0","unique_id":"mRQ0o7","updated_at":"2020-05-07 15:45:58","view_count":"23"},{"comment_count":"0","created_at":"2020-04-22 07:44:40","created_at_timestamp":"1587566680","is_commented":"0","is_liked":"0","is_shared":"0","is_viewed":"1","like_count":"0","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"","post_created_user_id":"1085","post_created_user_role":"App","post_id":"1471","post_media":[{"media_height":1234,"media_type":"image","media_url":"https://img.theweek.in/content/dam/week/news/biztech/2017/november/kids.jpg","media_width":599},{"media_height":1234,"media_type":"image","media_url":"https://static.toiimg.com/photo/msid-52685364/52685364.jpg?133438","media_width":599},{"media_height":1234,"media_type":"image","media_url":"https://i.pinimg.com/originals/4f/e0/0f/4fe00f66a018c2caa697958f9bf08ea0.jpg","media_width":599}],"post_type":"1","post_user_id":"1085","post_user_name":"Anuj Singhal","post_user_pic":"https://static.toiimg.com/photo/msid-64906749/64906749.jpg?resizemode=4&width=400","post_user_role":"App","share_count":"0","share_type":"0","unique_id":"geGTI0","updated_at":"2020-05-07 15:45:58","view_count":"23"},{"comment_count":"0","created_at":"2020-04-22 07:43:01","created_at_timestamp":"1587566581","is_commented":"0","is_liked":"0","is_shared":"0","is_viewed":"1","like_count":"0","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"Testing image.","post_created_user_id":"1085","post_created_user_role":"App","post_id":"1470","post_media":[{"media_height":1234,"media_type":"image","media_url":"https://i.pinimg.com/originals/81/56/be/8156be8f6a97290d5299eaa8782aab69.jpg","media_width":599}],"post_type":"1","post_user_id":"1085","post_user_name":"Test User","post_user_pic":"https://www.bridgeindia.org.uk/wp-content/uploads/2019/05/Kamini-Gupta.xa691a87f.jpg","post_user_role":"App","share_count":"0","share_type":"0","unique_id":"5WMebm","updated_at":"2020-05-07 15:45:58","view_count":"23"},{"comment_count":"1","created_at":"2020-04-15 08:33:13","created_at_timestamp":"1586964793","is_commented":"0","is_liked":"0","is_shared":"0","is_viewed":"1","like_count":"2","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"Very long content: Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum","post_created_user_id":"1084","post_created_user_role":"App","post_id":"1469","post_type":"1","post_user_id":"1084","post_user_name":"Utsav Singhal","post_user_pic":"https://cms.qz.com/wp-content/uploads/2019/07/IMG_8407-copy-e1563479651241.jpg?quality=75&strip=all&w=1600&h=900&crop=1","post_user_role":"App","share_count":"0","share_type":"0","unique_id":"TxqtD0","updated_at":"2020-05-07 15:45:58","view_count":"24"},{"comment_count":"0","created_at":"2020-04-15 08:23:46","created_at_timestamp":"1586964226","is_commented":"0","is_liked":"0","is_shared":"0","is_viewed":"1","like_count":"1","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"Test onspot video and preview","post_created_user_id":"1084","post_created_user_role":"App","post_id":"1468","post_type":"1","post_user_id":"1084","post_user_name":"Sandhya Singh","post_user_pic":"https://whats42nite.com/production/images/users/1084/I91wBUCK6g","post_user_role":"App","share_count":"0","share_type":"0","unique_id":"8w4BD6","updated_at":"2020-05-07 15:45:58","view_count":"24"},{"comment_count":"1","created_at":"2020-04-13 10:48:57","created_at_timestamp":"1586800137","is_commented":"0","is_liked":"1","is_shared":"0","is_viewed":"1","like_count":"4","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"1","post_content":"Test","post_created_user_id":"1084","post_created_user_role":"App","post_id":"1467","post_type":"1","post_user_id":"1084","post_user_name":"Shikha Goel","post_user_pic":"https://i.pinimg.com/originals/53/74/d8/5374d86eec8dcc22c8373a7648b203c2.jpg","post_user_role":"App","share_count":"1","share_type":"0","unique_id":"GHATBt","updated_at":"2020-05-07 15:45:59","view_count":"24"},{"comment_count":"3","created_at":"2020-04-10 15:06:28","created_at_timestamp":"1586556388","is_commented":"0","is_liked":"0","is_shared":"0","is_viewed":"1","like_count":"2","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"","post_created_user_id":"455","post_created_user_role":"App","post_id":"1466","post_media":[{"media_type":"image","media_url":"https://i.pinimg.com/originals/7f/92/88/7f9288e718fef2680d189ac508c7bd08.jpg"},{"media_height":400,"media_type":"image","media_url":"https://i.pinimg.com/originals/16/fa/ba/16fabad4de5cbc9f3609c9f6e71c8876.jpg","media_width":400}],"post_type":"1","post_user_id":"455","post_user_name":"Pradeep Kumar","post_user_pic":"https://i.pinimg.com/280x280_RS/c5/ce/bf/c5cebf50917f21b62654794951478e53.jpg","post_user_role":"App","share_count":"1","share_type":"0","unique_id":"9chn02","updated_at":"2020-05-07 15:45:59","view_count":"24"},{"comment_count":"2","created_at":"2020-04-10 15:05:45","created_at_timestamp":"1586556345","is_commented":"0","is_liked":"0","is_shared":"0","is_viewed":"1","like_count":"1","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"image test","post_created_user_id":"455","post_created_user_role":"App","post_id":"1465","post_media":[{"media_type":"image","media_url":"https://i.pinimg.com/originals/cf/d2/d3/cfd2d3d03bf4d3557c376d6c6c228d35.jpg"}],"post_type":"1","post_user_id":"455","post_user_name":"Test User","post_user_pic":"https://i.pinimg.com/originals/79/b0/d2/79b0d2b5f81d84e7f39558d4b30d1323.jpg","post_user_role":"App","share_count":"1","share_type":"0","unique_id":"P3wo9N","updated_at":"2020-05-07 15:45:59","view_count":"24"},{"comment_count":"2","created_at":"2020-04-10 06:52:56","created_at_timestamp":"1586526776","is_commented":"1","is_liked":"0","is_shared":"0","is_viewed":"1","like_count":"1","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"1","post_content":"Testing time zones.","post_created_user_id":"985","post_created_user_role":"VenueOwner","post_id":"1464","post_type":"1","post_user_id":"410","post_user_name":"Anand Singhal","post_user_pic":"https://i.pinimg.com/originals/d3/72/f3/d372f32afd3af173cb50fc5a6d1eceea.jpg","post_user_role":"venue","share_count":"1","share_type":"0","unique_id":"q8krw8","updated_at":"2020-05-07 15:45:59","view_count":"24"},{"comment_count":"1","created_at":"2020-04-09 08:09:12","created_at_timestamp":"1586444952","is_commented":"1","is_liked":"0","is_shared":"0","is_viewed":"1","like_count":"0","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"","post_created_user_id":"2","post_created_user_role":"Admin","post_id":"1463","post_media":[{"media_height":809,"media_type":"image","media_url":"https://tse1.mm.bing.net/th?id=OIP.5Emj0pY2FiTssBpjm1DykwAAAA&pid=15.1","media_width":1079}],"post_type":"1","post_user_id":"298","post_user_name":"Test User","post_user_pic":"https://i.pinimg.com/474x/84/5c/f1/845cf121f5081e65aa09a978e7452b0a.jpg","post_user_role":"venue","share_count":"0","share_type":"0","unique_id":"q8j0rc","updated_at":"2020-05-07 15:45:59","view_count":"24"},{"comment_count":"0","created_at":"2020-04-09 08:08:17","created_at_timestamp":"1586444897","is_commented":"0","is_liked":"1","is_shared":"1","is_viewed":"1","like_count":"1","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"","post_created_user_id":"2","post_created_user_role":"Admin","post_id":"1462","post_media":[{"media_height":809,"media_type":"image","media_url":"https://i.pinimg.com/736x/a3/0a/ff/a30aff95f37ca71c2d21f93f9280a9db.jpg","media_width":1079}],"post_type":"1","post_user_id":"298","post_user_name":"Test User","post_user_pic":"https://i.pinimg.com/originals/3f/68/3b/3f683b4e00b5602375838161d0688a89.jpg","post_user_role":"venue","share_count":"1","share_type":"0","unique_id":"q8j0pt","updated_at":"2020-05-07 15:45:59","view_count":"24"},{"comment_count":"0","created_at":"2020-04-08 07:23:40","created_at_timestamp":"1586355820","is_commented":"0","is_liked":"0","is_shared":"0","is_viewed":"1","like_count":"1","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"","post_created_user_id":"455","post_created_user_role":"App","post_id":"1461","post_media":[{"media_height":400,"media_type":"image","media_url":"https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT7Rxk164hmNr9OL9OFuS6e9CM6uALevX57q4OxgV8IRNEzfiEx&usqp=CAU","media_width":400}],"post_type":"1","post_user_id":"455","post_user_name":"Manish Gupta","post_user_pic":"https://i.pinimg.com/originals/d2/c4/e0/d2c4e09bbf8f18f29e62b77eae15efcd.jpg","post_user_role":"App","share_count":"0","share_type":"0","unique_id":"48mRKs","updated_at":"2020-05-07 15:45:59","view_count":"24"},{"comment_count":"1","created_at":"2020-04-07 12:02:38","created_at_timestamp":"1586286158","is_commented":"0","is_liked":"0","is_shared":"0","is_viewed":"1","like_count":"0","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"1","post_content":"post","post_created_user_id":"455","post_created_user_role":"App","post_id":"1460","post_type":"1","post_user_id":"455","post_user_name":"Alok Gupta","post_user_pic":"https://image.freepik.com/free-photo/indian-college-boy-with-bag_54391-1400.jpg","post_user_role":"App","share_count":"0","share_type":"0","unique_id":"zBihOc","updated_at":"2020-05-07 15:45:59","view_count":"24"},{"comment_count":"3","created_at":"2020-03-28 06:13:47","created_at_timestamp":"1585401227","is_commented":"0","is_liked":"0","is_shared":"0","is_viewed":"1","like_count":"1","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"","post_created_user_id":"2","post_created_user_role":"Admin","post_id":"1459","post_media":[{"media_height":251,"media_type":"image","media_url":"https://image.freepik.com/free-photo/young-indian-college-student-india_75648-285.jpg","media_width":201}],"post_type":"1","post_user_id":"298","post_user_name":"Avinash Singh","post_user_pic":"https://i.pinimg.com/originals/7a/f0/3d/7af03d4c296d1520decb678eb6321e5f.jpg","post_user_role":"venue","share_count":"2","share_type":"0","unique_id":"q7wnez","updated_at":"2020-05-07 15:45:59","view_count":"26"},{"comment_count":"0","created_at":"2020-03-28 06:12:06","created_at_timestamp":"1585401126","is_commented":"0","is_liked":"1","is_shared":"0","is_viewed":"1","like_count":"1","loc_address":"","loc_latitude":"","loc_longitude":"","loc_name":"","mime_type":"2","post_content":"","post_created_user_id":"2","post_created_user_role":"Admin","post_id":"1458","post_media":[{"media_height":201,"media_type":"image","media_url":"https://i.pinimg.com/originals/60/ab/56/60ab56ea8c71c13f340e792d3c3e283e.jpg","media_width":251}],"post_type":"1","post_user_id":"298","post_user_name":"Test User","post_user_pic":"https://image.freepik.com/free-photo/young-indian-college-student-india_75648-285.jpg","post_user_role":"venue","share_count":"0","share_type":"0","unique_id":"q7wnc6","updated_at":"2020-05-07 15:45:59","view_count":"26"}]
     * message : Data fetched successfully
     * pagination : {"next":"2020-04-30 21:05:46","previous":"2020-03-28 06:12:06"}
     * status : success
     */

    private AppVersionBean app_version;
    private String message;
    private PaginationBean pagination;
    private String status;
    private List<DataBean> data;

    public AppVersionBean getApp_version() {
        return app_version;
    }

    public void setApp_version(AppVersionBean app_version) {
        this.app_version = app_version;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PaginationBean getPagination() {
        return pagination;
    }

    public void setPagination(PaginationBean pagination) {
        this.pagination = pagination;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class AppVersionBean {
        /**
         * android : 9
         * ios : 8
         * is_force_update : 0
         */

        private String android;
        private String ios;
        private String is_force_update;

        public String getAndroid() {
            return android;
        }

        public void setAndroid(String android) {
            this.android = android;
        }

        public String getIos() {
            return ios;
        }

        public void setIos(String ios) {
            this.ios = ios;
        }

        public String getIs_force_update() {
            return is_force_update;
        }

        public void setIs_force_update(String is_force_update) {
            this.is_force_update = is_force_update;
        }
    }

    public static class PaginationBean {
        /**
         * next : 2020-04-30 21:05:46
         * previous : 2020-03-28 06:12:06
         */

        private String next;
        private String previous;

        public String getNext() {
            return next;
        }

        public void setNext(String next) {
            this.next = next;
        }

        public String getPrevious() {
            return previous;
        }

        public void setPrevious(String previous) {
            this.previous = previous;
        }
    }

    public static class DataBean {
        /**
         * comment_count : 3
         * created_at : 2020-04-30 21:05:46
         * created_at_timestamp : 1588305946
         * is_commented : 1
         * is_liked : 1
         * is_shared : 1
         * is_viewed : 1
         * like_count : 2
         * loc_address :
         * loc_latitude :
         * loc_longitude :
         * loc_name :
         * mime_type : 2
         * post_content : Testing
         * post_created_user_id : 1084
         * post_created_user_role : App
         * post_id : 1477
         * post_media : [{"media_height":800,"media_type":"image","media_url":"https://images.newindianexpress.com/uploads/user/imagelibrary/2020/1/25/w900X450/Ministry-sports.jpg","media_width":599}]
         * post_type : 1
         * post_user_id : 1084
         * post_user_name : Utsav Singhal
         * post_user_pic : https://cms.qz.com/wp-content/uploads/2019/07/IMG_8407-copy-e1563479651241.jpg?quality=75&strip=all&w=1600&h=900&crop=1
         * post_user_role : App
         * share_count : 21
         * share_type : 0
         * unique_id : ROf7sl
         * updated_at : 2020-05-08 05:12:06
         * view_count : 22
         */

        private String comment_count;
        private String created_at;
        private String created_at_timestamp;
        private String is_commented;
        private String is_liked;
        private String is_shared;
        private String is_viewed;
        private String like_count;
        private String loc_address;
        private String loc_latitude;
        private String loc_longitude;
        private String loc_name;
        private String mime_type;
        private String post_content;
        private String post_created_user_id;
        private String post_created_user_role;
        private String post_id;
        private String post_type;
        private String post_user_id;
        private String post_user_name;
        private String post_user_pic;
        private String post_user_role;
        private String share_count;
        private String share_type;
        private String unique_id;
        private String updated_at;
        private String view_count;
        private List<PostMediaBean> post_media;
        public int currentMediaItem;

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }

        public String getCreated_at() {
            return created_at;
        }

        public void setCreated_at(String created_at) {
            this.created_at = created_at;
        }

        public String getCreated_at_timestamp() {
            return created_at_timestamp;
        }

        public void setCreated_at_timestamp(String created_at_timestamp) {
            this.created_at_timestamp = created_at_timestamp;
        }

        public String getIs_commented() {
            return is_commented;
        }

        public void setIs_commented(String is_commented) {
            this.is_commented = is_commented;
        }

        public String getIs_liked() {
            return is_liked;
        }

        public void setIs_liked(String is_liked) {
            this.is_liked = is_liked;
        }

        public String getIs_shared() {
            return is_shared;
        }

        public void setIs_shared(String is_shared) {
            this.is_shared = is_shared;
        }

        public String getIs_viewed() {
            return is_viewed;
        }

        public void setIs_viewed(String is_viewed) {
            this.is_viewed = is_viewed;
        }

        public String getLike_count() {
            return like_count;
        }

        public void setLike_count(String like_count) {
            this.like_count = like_count;
        }

        public String getLoc_address() {
            return loc_address;
        }

        public void setLoc_address(String loc_address) {
            this.loc_address = loc_address;
        }

        public String getLoc_latitude() {
            return loc_latitude;
        }

        public void setLoc_latitude(String loc_latitude) {
            this.loc_latitude = loc_latitude;
        }

        public String getLoc_longitude() {
            return loc_longitude;
        }

        public void setLoc_longitude(String loc_longitude) {
            this.loc_longitude = loc_longitude;
        }

        public String getLoc_name() {
            return loc_name;
        }

        public void setLoc_name(String loc_name) {
            this.loc_name = loc_name;
        }

        public String getMime_type() {
            return mime_type;
        }

        public void setMime_type(String mime_type) {
            this.mime_type = mime_type;
        }

        public String getPost_content() {
            return post_content;
        }

        public void setPost_content(String post_content) {
            this.post_content = post_content;
        }

        public String getPost_created_user_id() {
            return post_created_user_id;
        }

        public void setPost_created_user_id(String post_created_user_id) {
            this.post_created_user_id = post_created_user_id;
        }

        public String getPost_created_user_role() {
            return post_created_user_role;
        }

        public void setPost_created_user_role(String post_created_user_role) {
            this.post_created_user_role = post_created_user_role;
        }

        public String getPost_id() {
            return post_id;
        }

        public void setPost_id(String post_id) {
            this.post_id = post_id;
        }

        public String getPost_type() {
            return post_type;
        }

        public void setPost_type(String post_type) {
            this.post_type = post_type;
        }

        public String getPost_user_id() {
            return post_user_id;
        }

        public void setPost_user_id(String post_user_id) {
            this.post_user_id = post_user_id;
        }

        public String getPost_user_name() {
            return post_user_name;
        }

        public void setPost_user_name(String post_user_name) {
            this.post_user_name = post_user_name;
        }

        public String getPost_user_pic() {
            return post_user_pic;
        }

        public void setPost_user_pic(String post_user_pic) {
            this.post_user_pic = post_user_pic;
        }

        public String getPost_user_role() {
            return post_user_role;
        }

        public void setPost_user_role(String post_user_role) {
            this.post_user_role = post_user_role;
        }

        public String getShare_count() {
            return share_count;
        }

        public void setShare_count(String share_count) {
            this.share_count = share_count;
        }

        public String getShare_type() {
            return share_type;
        }

        public void setShare_type(String share_type) {
            this.share_type = share_type;
        }

        public String getUnique_id() {
            return unique_id;
        }

        public void setUnique_id(String unique_id) {
            this.unique_id = unique_id;
        }

        public String getUpdated_at() {
            return updated_at;
        }

        public void setUpdated_at(String updated_at) {
            this.updated_at = updated_at;
        }

        public String getView_count() {
            return view_count;
        }

        public void setView_count(String view_count) {
            this.view_count = view_count;
        }

        public List<PostMediaBean> getPost_media() {
            return post_media;
        }

        public void setPost_media(List<PostMediaBean> post_media) {
            this.post_media = post_media;
        }

        public static class PostMediaBean {
            /**
             * media_height : 800
             * media_type : image
             * media_url : https://images.newindianexpress.com/uploads/user/imagelibrary/2020/1/25/w900X450/Ministry-sports.jpg
             * media_width : 599
             */

            private int media_height;
            private String media_type;
            private String media_url;
            private int media_width;

            public int getMedia_height() {
                return media_height;
            }

            public void setMedia_height(int media_height) {
                this.media_height = media_height;
            }

            public String getMedia_type() {
                return media_type;
            }

            public void setMedia_type(String media_type) {
                this.media_type = media_type;
            }

            public String getMedia_url() {
                return media_url;
            }

            public void setMedia_url(String media_url) {
                this.media_url = media_url;
            }

            public int getMedia_width() {
                return media_width;
            }

            public void setMedia_width(int media_width) {
                this.media_width = media_width;
            }
        }
    }
}
