package in.praxiv.fbtest;

import android.content.Context;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyCallback implements Callback<String> {

    private Context context;
    private ProgressBar progressBar;
    private ApiResponseListener listener;

    public MyCallback(Context context, ProgressBar progressBar, ApiResponseListener listener){
        this.context = context;
        this.progressBar = progressBar;
        this.listener = listener;
    }

    @Override
    public void onResponse(Call<String> call, Response<String> response) {
        if(progressBar != null)
            progressBar.setVisibility(View.GONE);
        if(response.isSuccessful()){
            String responseBody = response.body();
            if(responseBody != null){
                listener.respond(responseBody);
            } else{
                Toast.makeText(context, "Data not available", Toast.LENGTH_SHORT).show();
            }
        } else{
            Toast.makeText(context, "Error: " + response.code(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onFailure(Call<String> call, Throwable t) {
        Toast.makeText(context, "Failure: " + t.toString(), Toast.LENGTH_SHORT).show();
        if(progressBar != null)
            progressBar.setVisibility(View.GONE);
    }
}
